<?php
	session_start();
	include '../../core/config.php';

	if($_SESSION["role"] == 0){
		$data = mysqli_query($conn,"SELECT * FROM tbl_classes as c INNER JOIN tbl_classes_student cs ON cs.class_id = c.class_id WHERE cs.added_by = '$_SESSION[uid]' GROUP BY c.date_added");
	}else{
		$data = mysqli_query($conn,"SELECT * FROM tbl_classes WHERE added_by = '$_SESSION[uid]' GROUP BY date_added");
	}
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$list = array();
		$list["count"] = $count++;
		$list["class_id"] = $row["class_id"];
		$list["year"] = date("Y", strtotime("-1 year",strtotime($row["date_added"])))." - ".date("Y", strtotime($row["date_added"]));
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>