<?php
	session_start();
	include '../../core/config.php';
	if($_SESSION["role"] == 0){
		$added = "cs.added_by = '$_SESSION[uid]'";
	}else{
		$added = "c.added_by = '$_SESSION[uid]'";
	}

	$data = mysqli_query($conn,"SELECT *, cs.added_by as s, c.added_by as t FROM tbl_classes_student as cs JOIN tbl_classes c ON cs.class_id = c.class_id WHERE $added");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$list = array();
		$list["count"] = $count++;
		$list["sclass_id"] = $row["sclass_id"];
		$list["class_id"] = $row["class_id"];
		$list["class_name"] = $row["class_name"];
		$list["teacher_name"] = getTeacherName($conn, $row["added_by"]);
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>