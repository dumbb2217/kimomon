<?php
	session_start();
	include '../../core/config.php';
	$s_id = $_POST["s_id"];
	$module_s = isset($_POST["pF"])?"AND content_type != 2":"";
	if($_SESSION["role"] == 1){
		$added = "AND added_by = '$_SESSION[uid]'".$module_s;
	}else{
		$added = "AND is_posted = 1";
	}

	$data = mysqli_query($conn,"SELECT * FROM tbl_modules WHERE subject_id = '$s_id' $added");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$list = array();
		$list["count"] = $count++;
		$list["module_id"] = $row["module_id"];
		$list["module_name"] = $row["content_type"] == 1?strtoupper($row["module_name"]):$row["content_type"] == 3?strtoupper($row["module_name"]):strtoupper($row["module_instructions"]);
		$list["module_deadline"] = $row["content_type"] == 1 || $row["content_type"] == 3?date("F d, Y", strtotime($row["module_deadline"])):"N/A";
		$list["date_added"] = $row["date_added"];
		$list["c_type"] = $row["content_type"] != 2 ?"Answer":"View";
		$list["type"] = $row["content_type"] == 1?"Module":($row["content_type"] == 2?"Material":"Activity");


		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>