<?php
	session_start();
	include '../../core/config.php';
	$m_id = $_POST["m_id"];

	function getScore($conn, $m_id, $uID){
		$ans_data = mysqli_fetch_array(mysqli_query($conn, "SELECT *, count(*) as total_q FROM tbl_student_answers WHERE module_id = '$m_id' AND user_id = '$uID'"));
		$correct_ans = mysqli_fetch_array(mysqli_query($conn, "SELECT sa.s_answer_id, count(*) as total_a FROM tbl_student_answers sa INNER JOIN tbl_module_answer ma ON ma.ma_id = sa.ma_id WHERE sa.module_id = '$m_id' AND sa.user_id = '$uID' AND ma.is_correct = 1"));
		$score = $correct_ans["total_a"]."/".$ans_data["total_q"];
		return $score;
	}

	function hasAnswered($conn, $m_id, $uID){
		$ans_data = mysqli_fetch_array(mysqli_query($conn, "SELECT *, count(*) as total_q FROM tbl_student_answers WHERE module_id = '$m_id' AND user_id = '$uID'"));
		if($ans_data["total_q"] != 0){
			$has_answered = "<i class='fa fa-check-circle text-success'></i>";
		}else{
			$has_answered = "<i class='fa fa-times-circle text-dark'></i>";
		}

		return $has_answered;
	}

	$data = mysqli_query($conn,"SELECT *, cs.added_by as student FROM tbl_classes_student cs JOIN tbl_classes c ON cs.class_id = c.class_id WHERE c.added_by = '$_SESSION[uid]'");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$mData = mysqli_fetch_array(mysqli_query($conn,"SELECT content_type FROM tbl_modules WHERE module_id = '$m_id'"));
		$viewActivity = "<button class='btn btn-outline-primary btn-sm' onclick='viewAct(".$m_id.")'>View</button>";

		$list = array();
		$list["count"] = $count++;
		$list["sclass_id"] = $row["sclass_id"];
		$list["student_name"] = strtoupper(getStudentName($conn, $row["student"]));
		$list["student_score"] = $mData[0] == 3?"":getScore($conn, $m_id, $row['student']);
		$list["has_answered"] = $mData[0] == 3?$viewActivity:hasAnswered($conn, $m_id, $row['student']);
		// $list["date_added"] = $row["date_added"];
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>