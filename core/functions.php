<?php

	function page_url($page){
		return md5(base64_encode($page));
	}

	function enCrypt($data){
		return base64_encode($data);
	}

	function deCrypt($data){
		return base64_decode($data);
	}

	function getTeacherName($conn, $uID){
		$data = mysqli_fetch_array(mysqli_query($conn,"SELECT name FROM tbl_users WHERE user_id = '$uID'"));

		return strtoupper($data[0]);
	}

	function getStudentName($conn, $uID){
		$data = mysqli_fetch_array(mysqli_query($conn,"SELECT name FROM tbl_users WHERE user_id = '$uID'"));

		return strtoupper($data[0]);
	}

	function getClassName($conn, $cID){
		$data = mysqli_fetch_array(mysqli_query($conn,"SELECT class_name FROM tbl_classes WHERE class_id = '$cID'"));

		return strtoupper($data[0]);
	}

	function getSubjectName($conn, $sID){
		$data = mysqli_fetch_array(mysqli_query($conn,"SELECT subject_name FROM tbl_subject WHERE subject_id = '$sID'"));

		return strtoupper($data[0]);
	}

?>