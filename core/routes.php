<?php
	
	$pages = $_GET["page"];
	if($pages == page_url("dashboard")){
		require '../main/pages/dashboard.php';
	}else if($pages == page_url("academic_year")){
		require '../main/pages/academic_year.php';
	}else if($pages == page_url("classes")){
		require '../main/pages/classes.php';
	}else if($pages == page_url("classes_details")){
		require '../main/pages/classes_details.php';
	}else if($pages == page_url("subject_details")){
		require '../main/pages/subject_details.php';
	}else if($pages == page_url("subject_modules")){
		require '../main/pages/subject_modules.php';
	}else if($pages == page_url("users")){
		require '../main/pages/users.php';
	}else if($pages == page_url("people")){
		require '../main/pages/people.php';
	}else if($pages == page_url("modules")){
		require '../main/pages/modules.php';
	}else if($pages == page_url("profile")){
		require '../main/pages/profile.php';
	}else if($pages == page_url("announcement")){
		require '../main/pages/announcement.php';
	}else if($pages == page_url("modules_add")){
		require '../main/pages/modules_add.php';
	}else if($pages == page_url("module_answer")){
		require '../main/pages/modules_answer.php';
	}else if($pages == page_url("modules_student_scores")){
		require '../main/pages/modules_student_scores.php';
	}else if($pages == page_url("academic_year_details")){
		require '../main/pages/academic_year_details.php';
	}else{
		require '../main/pages/404.php';
	}

?>