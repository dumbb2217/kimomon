<?php
  $s_id = $_GET["s_id"];
  $c_id = $_GET["c_id"];
  $m_id = $_GET["m_id"];
  $pF = $_GET["pF"];
  $row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_modules WHERE module_id = '$m_id'"));
  $hide_m = $row["content_type"] != 1?"style='display: none;'":"";
  $hide_a = $row["content_type"] != 3?"style='display: none;'":"";
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1><a href="index.php?page=<?=page_url('modules_add')?>&s_id=<?=$s_id?>&c_id=<?=$c_id?>&pF=<?=$pF?>"><i class="fa fa-chevron-left"></i> Subject details</a> / Module details</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title"><?=strtoupper($row["module_name"])?></h5>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <div class="row">
                    <div class="table-responsive col-8 offset-2">
                      <table id="tbl_students" class="table table-condensed">
                          <thead>
                          <tr>
                              <!-- <th style="width: 10px"><input type="checkbox" id="checkAllClass" onclick="checkAllClass()"></th> -->
                              <th style="width: 10px"></th>
                              <th>Student Name</th>
                              <th width="20"></th>
                              <th style="width: 10px"></th>
                          </tr>
                          </thead>
                          <tbody>
                          </tbody>
                      </table>
                      </div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
              <div class="card">
              <div class="card-header">
                <h5 class="card-title">Comments</h5>
              </div>
              <div class="card-body">
                <div class="col-12">
                  <div class="card-footer card-comments">
                    <?php
                      $getComment = mysqli_query($conn, "SELECT * FROM tbl_comment WHERE module_id = '$m_id'");
                      if(mysqli_num_rows($getComment) != 0){
                        while($row = mysqli_fetch_array($getComment)){
                    ?>
                    <div class="card-comment">
                      <!-- User image -->
                      <h1><i class="fa fa-user-circle img-circle img-sm"></i></h1>

                      <div class="comment-text pl-2">
                        <span class="username">
                          <?=strtoupper(getStudentName($conn, $row["added_by"]))?>
                          <span class="text-muted float-right pt-2"><?=date("F d, Y", strtotime($row["date_added"]))?></span>
                        </span><!-- /.username -->
                        <?=$row["comment"]?>
                      </div>
                      <!-- /.comment-text -->
                    </div>
                    <?php } }else{ ?>
                      <div class="text-center text-muted">No comment available</div>
                    <?php } ?>

                  </div>
                </div>
              </div>

            </div>
            </div>
            <!-- /.col -->
          </div>
        </div>
      </div>

      

      <div class="modal fade" id="view_act_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Activity list</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body act-md-body">
                  
            </div>
          </div>
        </div>
      </div>

    </section>

    <script type="text/javascript">
        $(document).ready( function(){
            get_student_scores();
        });

        function get_student_scores(){
            $("#tbl_students").DataTable().destroy();
            $("#tbl_students").dataTable({
            "ajax": {
                "type": "POST",
                "url": "../ajax/datatables/modules_summary_data.php",
                "data": {m_id: "<?=$m_id?>"}
            },
            "processing": true,
            "bSort": false,
            "paging": false,
            "info": false,
            "columns": [
            {
                "mRender": function(data, type, row){
                return "<i class='fa fa-user-circle ml-1'></i>";
                }
            },
            {
                "data": "student_name"
            },
            {
                "data": "student_score"
            },
            {
                "data": "has_answered"
            },
            ]
            });
        }

        function viewAct(mID){
          var url = "../ajax/activity_list.php";
          $.ajax({
          type: "POST",
          url: url,
          data: {mID: mID},
          success: function(data){
            if(data){
              $("#view_act_md").modal();
              $(".act-md-body").html(data);
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
        }
    </script>