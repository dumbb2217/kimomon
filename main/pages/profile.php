<?php
    $row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_users WHERE user_id = '$_SESSION[uid]'"));
    $selected = $row['sex'] == 1?"selected":"";
    $selected2 = $row['sex'] == 2?"selected":"";
?>
<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title">Personal Information</h5>
                    <!-- <div class="card-tools">
                      <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_st_class_md">
                        Add
                      </button>
                      <button type="button" class="btn btn-sm btn-danger" onclick="delete_student_class()">
                        Delete
                      </button>
                    </div> -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                        <form id="user_profile_form" class="col-8 offset-2">
                            <div class="row">
                                <div class="form-group col-6 mr-1">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="name" value="<?=$row['name']?>">
                                    <input type="hidden" class="form-control" name="user_id" value="<?=$row['user_id']?>">
                                </div>
                                <div class="form-group col-6">
                                    <label>Age</label>
                                    <input type="text" class="form-control" name="age" maxlength="2" value="<?=$row['age']?>">
                                </div>

                                <div class="form-group col-6">
                                    <label>Birthdate</label>
                                    <input type="date" class="form-control" name="bdate" value="<?=$row['bdate']?>">
                                </div>

                                <div class="form-group col-6 mr-1">
                                    <label>Sex</label>
                                    <select class="form-control" name="sex">
                                        <option value="0">Select sex:</option>
                                        <option value="1" <?=$selected?>>Male</option>
                                        <option value="2" <?=$selected2?>>Female</option>
                                    </select>
                                </div>

                                
                                <div class="form-group col-6">
                                    <label>Username / eMail Address</label>
                                    <input type="email" class="form-control" name="email" value="<?=$row['username']?>">
                                </div>

                                <div class="form-group col-6">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                                
                                <div class="form-group col-6">
                                    <label>Contact No.</label>
                                    <input type="text" class="form-control" name="contact" maxlength="11" value="<?=$row['contact_no']?>">
                                </div>
                                <div class="form-group col-12">
                                    <label>Address</label>
                                    <textarea class="form-control" name="address"><?=$row["address"]?></textarea>
                                </div>
                                <br>
                                <div class="btn-group col-3 offset-9">
                                    <button type="submit" class="btn btn-primary">Save Changes</button>
                                </div>
                            </div>
                        </form>
                  </div>
                </div>
                <!-- /.card -->
              </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <script type="text/javascript">
      $(document).ready( function(){
      });

      $("#user_profile_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/user_update.php";
        var conf = confirm("Changing profile information will make you logout. Will you proceed?");
        if(conf){
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(data){
                    if(data == 1){
                        alert("Success: Profile was updated.");
                        window.location.href="../ajax/logout.php";
                    }else{
                        alert("Error: Something was wrong.");
                    }
                }
            });
        }
      });
      
    </script>