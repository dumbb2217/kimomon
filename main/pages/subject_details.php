<?php
  $s_id = $_GET["s_id"];
  $c_id = $_GET["c_id"];
  $pF = $_GET["pF"];
  $row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_subject WHERE subject_id = '$s_id'"));
  $hide_s = $_SESSION['role'] == 0?"style='display:none;'":"";
  $hide = $_SESSION['role'] == 1?"style='display:none;'":"";
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1><a href="index.php?page=<?=page_url('classes_details')?>&c_id=<?=$c_id?>&pF=<?=$pF?>"><i class="fa fa-chevron-left"></i> Class Details</a> / Subject Details</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-12 mb-3">
            <h1>Subject: <?=strtoupper($row["subject_name"])?></h1>
            </div>
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title">Modules List</h5>
                  <div class="card-tools" <?=$hide_s?>>
                    <button type="button" class="btn btn-sm btn-success" onclick="add_module()">
                      Add
                    </button>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_module()">
                      Delete
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="tbl_modules" class="table table-condensed ">
                    <thead>
                      <tr>
                        <th style="width: 10px"><input type="checkbox" id="checkAllCB" onclick="checkAllCB()"></th>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th style="width: 80px">Type</th>
                        <th style="width: 150px">Deadline</th>
                        <th style="width: 100px">Date Added</th>
                        <th style="width: 100px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <script type="text/javascript">
      $(document).ready( function(){
        get_modules();
      });

      function get_modules(){
        $("#tbl_modules").DataTable().destroy();
        $("#tbl_modules").dataTable({
          "ajax": {
            "type": "POST",
            "url": "../ajax/datatables/modules_data.php",
            "data": {s_id: "<?=$s_id?>"}
          },
          "processing": true,
          "bPaginate": false,
          "bLengthChange": false,
          "bFilter": true,
          "bInfo": false,
          "sort": false,
          "columns": [
          {
            "mRender": function(data, type, row){
              return "<input type='checkbox' value='"+row.module_id+"' name='cb_modules'>";
            }
          },
          {
            "data": "count"
          },
          {
            "data": "module_name"
          },
          {
            "data": "type"
          },
          {
            "data": "module_deadline"
          },
          {
            "data": "date_added"
          },
          {
            "mRender": function(data, type, row){
              return "<button class='btn btn-sm btn-outline-dark btn-block' onclick='module_details("+row.module_id+")' <?=$hide_s?>>View details</button>"+
              "<button class='btn btn-sm btn-outline-dark btn-block' onclick='module_answer("+row.module_id+")' <?=$hide?>>"+row.c_type+"</button>";
            }
          }
          ]
        });
      }

      function checkAllCB(){
        var x = $("#checkAllCB").is(":checked");
        if(x){
          $("input[name=cb_modules]").prop("checked", true);
        }else{
          $("input[name=cb_modules]").prop("checked", false);
        }
      }

      function add_module(){
        var s_id = '<?=$s_id?>';
        var c_id = '<?=$c_id?>';
        var pF = "<?=$pF?>";
        window.location.href="index.php?page=<?=page_url('subject_modules')?>&s_id="+s_id+"&c_id="+c_id+"&pF="+pF;
      }

      function delete_module(){
        var conf = confirm("Are you sure to delete selected?");
        if(conf){
          var modules = [];
          $("input[name=cb_modules]:checked").each( function(){
            modules.push($(this).val());
          });

          if(modules.length != 0){
            var url = "../ajax/module_delete.php";
            $.ajax({
              type: "POST",
              url: url,
              data: {mID: modules},
              success: function(data){
                if(data != 0){
                  alert("Success: Selected module/s was removed.");
                  get_modules();
                }else{
                  alert("Error: Something is wrong.");
                }
              }
            });
          }else{
            alert("Warning: No data selected.");
          }
        }
      }

      function module_details(mID){
        var s_id = '<?=$s_id?>';
        var cID = '<?=$c_id?>';
        var pF = "<?=$pF?>";
        if(pF == "classes"){
          window.location.href="index.php?page=<?=page_url('subject_modules')?>&s_id="+s_id+"&m_id="+mID+"&c_id="+cID+"&pF="+pF;
        }else{
          window.location.href="index.php?page=<?=page_url('subject_modules')?>&s_id="+s_id+"&m_id="+mID+"&c_id="+cID+"&pF="+pF;
        }
      }

      function module_answer(mID){
        var s_id = '<?=$s_id?>';
        var cID = '<?=$c_id?>';
        var pF = "<?=$pF?>";
        window.location.href="index.php?page=<?=page_url('module_answer')?>&s_id="+s_id+"&m_id="+mID+"&c_id="+cID+"&pF="+pF;
      }
    </script>