<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-8">
            <h1>Users Management</h1>
          </div>
          <div class="col-sm-2 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

        <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title">Users List</h5>
                  <div class="card-tools">
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_user_md">
                      Add
                    </button>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_user()">
                      Delete
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="tbl_branch" class="table table-condensed table-bordered">
                    <thead>
                      <tr>
                        <th style="width: 10px"><input type="checkbox" id="checkAllUser" onclick="checkAllUser()"></th>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th style="width: 350px">Username</th>
                        <th style="width: 350px">Branch</th>
                        <th style="width: 100px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <!-- Add Modal -->
    <div class="modal fade" id="add_user_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="add_user_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name" class="form-control" placeholder="Name" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Username</label>
                  <input type="text" name="username" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Branch</label>
                  <select name="branch_id" id="branch_id1" class="form-control" required="">
                    <option value="0">Select Branch:</option>
                    <?=getBranchList($conn);?>
                  </select>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="edit_user_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="edit_user_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name" id="name" class="form-control" placeholder="Name" required="">
                  <input type="hidden" name="user_id" id="user_id">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Username</label>
                  <input type="text" name="username" id="username" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Branch</label>
                  <select name="branch_id" id="branch_id" class="form-control" required="">
                    <option value="0">Select Branch:</option>
                    <?=getBranchList($conn);?>
                  </select>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $(document).ready( function(){
        get_user();
      });

      function get_user(){
        $("#tbl_branch").DataTable().destroy();
        $("#tbl_branch").dataTable({
          "ajax": {
            "type": "POST",
            "url": "../ajax/datatables/users_data.php",
          },
          "processing": true,
          "columns": [
          {
            "mRender": function(data, type, row){
              return "<input type='checkbox' value='"+row.user_id+"' name='cb_user'>";
            }
          },
          {
            "data": "count"
          },
          {
            "data": "name"
          },
          {
            "data": "username"
          },
          {
            "data": "branch"
          },
          {
            "mRender": function(data, type, row){
              return "<button class='btn btn-sm btn-outline-dark' onclick='edit_user("+row.user_id+")'>Edit User</button>";
            }
          }
          ]
        });
      }

      function checkAllUser(){
        var x = $("#checkAllUser").is(":checked");
        if(x){
          $("input[name=cb_user]").prop("checked", true);
        }else{
          $("input[name=cb_user]").prop("checked", false);
        }
      }

      $("#add_user_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/user_add.php";
        var branch_id =  $("#branch_id1").val();
        if(branch_id != 0){
          $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(data){
              if(data == 1){
                alert("Success: New user was added.");
                $("#add_user_md").modal("hide");
                $("input").val("");
                $("select").val("");
                get_user();
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }else{
          alert("Warning: No branch was selected.");
        }
      });

      function edit_user(uID){
        var url = "../ajax/user_details.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {uID: uID},
          success: function(data){
            $("#edit_user_md").modal();
            var o = JSON.parse(data);
            $("#user_id").val(uID);
            $("#name").val(o.name);
            $("#username").val(o.username);
            $("#password").val("");
            $("#branch_id").val(o.branch_id);
          }
        });
      }

      $("#edit_user_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/user_update.php";
        var branch_id =  $("#branch_id").val();
        if(branch_id != 0){
          $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(data){
              if(data == 1){
                alert("Success: User details was updated.");
                $("#edit_user_md").modal("hide");
                $("input").val("");
                $("select").val("");
                get_user();
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }else{
          alert("Warning: No branch was selected.");
        }
      });

      function delete_user(){
        var conf = confirm("Are you sure to delete selected?");
        if(conf){
          var users = [];
          $("input[name=cb_user]:checked").each( function(){
            users.push($(this).val());
          });

          if(users.length != 0){
            var url = "../ajax/user_delete.php";
            $.ajax({
              type: "POST",
              url: url,
              data: {uID: users},
              success: function(data){
                if(data != 0){
                  alert("Success: Selected User/s was removed.");
                  get_user();
                }else{
                  alert("Error: Something is wrong.");
                }
              }
            });
          }else{
            alert("Warning: No data selected.");
          }
        }
      }
    </script>