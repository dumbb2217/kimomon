<?php include 'core/config.php'; ?>
<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <title>KiMoMon | Sign up</title>

	  <!-- Google Font: Source Sans Pro -->
	  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	  <!-- Font Awesome -->
	  <link rel="stylesheet" href="assets/plugins/fontawesome-free/css/all.min.css">
	  <!-- icheck bootstrap -->
	  <link rel="stylesheet" href="assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	  <!-- Theme style -->
	  <link rel="stylesheet" href="assets/dist/css/adminlte.min.css">
	  <style type="text/css">
	  	.login-page, .register-page {
	  		justify-content: unset !important;
	  		-webkit-justify-content: unset !important;
	  		padding-top: 8%;
	  	}
	  </style>
	</head>
	<body class="hold-transition register-page">
		<div class="register-box">
		  <div class="card card-outline card-primary">
		    <div class="card-header text-center">
		      <a href="index.php" class="h1"><b>Ki</b>MoMon</a>
		    </div>
		    <div class="card-body">
		      <p class="login-box-msg">Register a new membership</p>

		      <form id="formSignup" action="#" method="post">
		      	<div class="form-group mb-3">
	                <label>I am a</label>
	                <select class="custom-select form-control" id="user_type" name="user_type" onchange="u_type()" required="">
	                  <option value="0">Student</option>
	                  <option value="1">Teacher</option>
	                </select>
	              </div>
		        <div class="input-group mb-3">
		          <input type="text" class="form-control" placeholder="Full name" name="fname" required="">
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <span class="fas fa-user"></span>
		            </div>
		          </div>
		        </div>
		        <div class="input-group mb-3 school-inp" style="display: none;">
		          <input type="text" class="form-control" placeholder="School" name="school">
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <span class="fas fa-book"></span>
		            </div>
		          </div>
		        </div>
		        <div class="input-group mb-3">
		          <input type="email" class="form-control" placeholder="Email" name="uname" required="">
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <span class="fas fa-envelope"></span>
		            </div>
		          </div>
		        </div>
		        <div class="input-group mb-3">
		          <input type="password" class="form-control" placeholder="Password" name="pass" required="">
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <span class="fas fa-lock"></span>
		            </div>
		          </div>
		        </div>
		        <div class="input-group mb-3">
		          <input type="password" class="form-control" placeholder="Retype password" name="pass1" required="">
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <span class="fas fa-lock"></span>
		            </div>
		          </div>
		        </div>
		        <div class="row">
		          <!-- /.col -->
		          <div class="col-4 offset-8">
		            <button type="submit" class="btn btn-primary btn-block btn-signup">Register</button>
		          </div>
		          <!-- /.col -->
		        </div>
		      </form>

		      <a href="index.php" class="text-center">I already have a membership</a>
		    </div>
		    <!-- /.form-box -->
		  </div><!-- /.card -->
		</div>
		<!-- /.register-box -->

	<!-- jQuery -->
	<script src="assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="assets/dist/js/adminlte.min.js"></script>
	</body>
</html>

<script type="text/javascript">

	function u_type(){
		var user_type = $("#user_type").val();
		if(user_type == 1){
			$(".school-inp").show();
		}else{
			$(".school-inp").hide();
		}
	}

	$("#formSignup").submit( function(e){
		e.preventDefault();
		$(".btn-signup").prop("disabled", true);

		var data = $(this).serialize();
		setTimeout( function(){
			$.ajax({
				type: "POST",
				url: "ajax/register.php",
				data: data,
				success: function(data){
					if(data == 1){
						alert("Account registration successful!\nYou may now signin.");
						window.location="index.php";
					}else{
						alert("Error: Username or password incorrect.");
						$(".btn-signup").prop("disabled", false);
						$(".btn-signup").html("Register");
					}
				}
			});
		},2000);
	});
</script>